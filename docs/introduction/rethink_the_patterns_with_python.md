# Repensando os Patterns com Python
Em linguagens dinâmicas como _Python_, _Ruby_  e _JavaScript_ alguns design patterns tradicionais devem ser repensados[^1], visto que certas caracteristicas dessas linguagens podem simplificar bastante os patterns ou tornalos invisiveis.

## Factory
Em python os patterns _criacionais_ não são muito utilizados pois o mecanismo de `factory` já está pronto na linguagem [^2] e pode ser sobrescrito através da sobreposição do método `__new__`.

```python
class Foo:

    def __new__(cls):
        print('Esse é o construtor da classe')
        return super().__new__(cls)

	def __init__(self):
		print('Essa é a iniciação da classe')
```
O método `__new__` é invocado pelo python sempre que uma nova instância de uma determinada classe for criada e o retorno desse método deverá ser a instância da classe em questão.
Isso permite determinar a forma como objetos serão criados sem que seja necessário criar métodos ou mecanismos especiais para isso.
Em outras linguagens como por exemplo o Java, o método construtor é invocado depois que a instancia já foi criada (equivalente ao método especial `__init__` do python), dessa forma, não é possível intervir na forma como essa instancia foi criada.
Outra desvantagem de linguagens como o Java é a necessidade do uso do operador `new`, que força a criação de objetos a partir de classes concretas.

```java
Foo foo = new Foo();
```
Em python não se usa o operador `new`, o que torna o código mais flexível já que `Foo()` pode ser uma classe concreta ou um `factory` (inclusive pode ser uma função e não uma classe).

```python
foo = Foo()
```

## First-Class types

## Herança multipla

[^1]:
	Design Patterns in Dynamic Programming.
	Peter Norvig.
	http://norvig.com/design-patterns/design-patterns.pdf
[^2]:
	Google Developers Day US - Python Design Patterns.
	Alex Martelli.
	https://www.youtube.com/watch?v=0vJJlVBVTFg
